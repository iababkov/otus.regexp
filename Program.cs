﻿using System;

namespace OtusReqularExpr
{
    class Program
    {
        static void Main(string[] args)
        {
            const string url = "https://www.google.ru/";
            const string srcAttribute = "src=";
            string srcValue;

            UrlReader urlReader = new UrlReader();
            TextProcessor textProcessor = new(urlReader.GetHtmlFromUrl(url));
            FileProcessor fileProcessor = new(@"C:\Users\Public");

            foreach (string result in textProcessor.FindByRegEx(@"<img\s.*?src="".*?"""))
            { 
                srcValue = result.Substring(result.IndexOf(srcAttribute) + srcAttribute.Length).Trim('"');

                Console.WriteLine(srcValue);

                fileProcessor.SaveAs(urlReader.GetFileDataFromUrl(srcValue.IndexOf("//") < 0 ? url + srcValue : srcValue), srcValue.Substring(srcValue.LastIndexOf('/')));
            }
        }
    }
}
