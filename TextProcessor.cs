﻿using System.Collections;
using System.Text.RegularExpressions;

namespace OtusReqularExpr
{
    public class TextProcessor
    {
        public string Text { get; set; }

        public TextProcessor(string text)
        {
            Text = text;
        }

        public IEnumerable FindByRegEx(string regExPattern)
        {
            if (Text == null)
            {
                yield break;
            }
            else
            {
                Regex rgx = new Regex(regExPattern);

                foreach(Match match in rgx.Matches(Text))
                {
                    yield return match.Value;
                }
            }
        }
    }
}
