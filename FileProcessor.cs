﻿using System.IO;

namespace OtusReqularExpr
{
    public class FileProcessor
    {
        const char folderDelimiter = '\\';

        public string Folder { get; set; }

        public FileProcessor(string folder)
        {
            Folder = folder;
        }

        public void SaveAs(byte[] byteArray, string fileName)
        {
            File.WriteAllBytes(Folder + folderDelimiter + fileName, byteArray);
        }
    }
}
