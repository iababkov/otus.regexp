﻿using System.Collections.Generic;
using System.IO;
using System.Net;

namespace OtusReqularExpr
{
    public class UrlReader
    {
        public int RequestTimeOutInSec { get; set; }
        private readonly Dictionary<string, string> _urlHtmlCache = new Dictionary<string, string>();

        public UrlReader()
        {
            RequestTimeOutInSec = 10;
        }

        public UrlReader(int requestTimeOutInSec)
        {
            RequestTimeOutInSec = requestTimeOutInSec;
        }

        public string GetHtmlFromUrl(string url)
        {
            if (!_urlHtmlCache.ContainsKey(url))
            {
                var request = WebRequest.Create(url);
                request.Timeout = RequestTimeOutInSec * 1000;

                using var reader = new StreamReader(request.GetResponse().GetResponseStream());

                _urlHtmlCache[url] = reader.ReadToEnd();
            }

            return _urlHtmlCache[url];
        }

        public byte[] GetFileDataFromUrl(string url)
        {
            var request = WebRequest.Create(url);
            request.Timeout = RequestTimeOutInSec * 1000;

            using var response = request.GetResponse();
            long bytesCount = response.ContentLength;
            byte[] byteArray = new byte[bytesCount];
            using var reader = new BinaryReader(response.GetResponseStream());
            byteArray = reader.ReadBytes((int)bytesCount);

            return byteArray;
        }
    }
}

